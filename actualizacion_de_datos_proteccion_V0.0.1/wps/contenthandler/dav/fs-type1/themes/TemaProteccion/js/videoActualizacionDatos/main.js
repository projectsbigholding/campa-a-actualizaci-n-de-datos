/*
  author: Jorge Alejandro Benjumea
  email: j.alejandro.benjumea@gmail.com
  Last update: 2016/11/24 14:34:54
*/
/*
  @author: Jorge Alejandro Benjumea Giraldo
  @email: j.alejandro.benjumea@gmail.com
*/

$(function() {

  'use strict';

  $.ajax({
    url: 'udc.html',
    type: 'GET',
    dataType: 'html',
    success: function(data) {
      $(data).insertBefore($('body > script').eq(0));
    },
    error: function() {
      console.error('Update Data Campaign template not found');
    }
  });

});

/*
  @author: Jorge Alejandro Benjumea Giraldo
  @email: j.alejandro.benjumea@gmail.com
*/

// Grunt JSHINT and JSCS configuration
  /* globals TimelineMax, TweenLite, Power2, Power3 */
  // jscs:disable maximumLineLength
  // jscs:disable disallowMultipleVarDecl

(function($) {
  'use strict';

  var updateData, loadCampaign, base;

  base = 'wps/contenthandler/dav/fs-type1/themes/TemaProteccion/js/videoActualizacionDatos/data/';

  updateData = function(userData) {

    var options = {
      mobileSection: '.udc-mobile',
      desktopSection: '.udc-player',
      slider: {
        target: '.udc-mobile-carousel',
        options: {
          prevArrow: '.btns .btn-prev',
          nextArrow: '.btns .btn-continue'
        }
      },
      player: {
        videoId: 'IrrI37HPtn8',
        audioSrc: 'wps/contenthandler/dav/fs-type1/themes/TemaProteccion/js/videoActualizacionDatos/player/proteccion.mp3'
      },
      dontShowAgain: function() {
        // 'No volver a mostrar' Button behavior
      },
      seeLater: function() {
        var _ = this;
        // 'Ver más tarde' Button behavior
        $('<h2><a style="color: blue; cursor:pointer;">Volver a mostrar</a><h2>')
          .on('click', function() {
            $(this).remove();
            _.show();
          })
          .insertAfter('.developer h1');
      },
      form: {
        data: {
          residencia: {
            ciudad: null,
            departamento: null,
            direccion: null,
          },
          telefono: null,
          celular: null,
          correoElectronico: null,
          autorizacion: {
            mail: false,
            sms: false
          },
          financiero: {
            conceptoIngresos: [],
            ingresos: null,
            egresos: null,
            totalActivos: null,
            totalPasivos: null,
            ocupacion: null,
            profesion: null
          }
        },
        httpRequest: [
          {
            url: base + 'regions.json',
            afterResponse: function(response) {
              var _ = this, $states, regions, userState, userCity;

              $states = $('.states');

              regions = response.regiones.map(function(region) {return {id: region.id.value, nombre: region.nombre.value};});
              regions.forEach(function(region) {
                $states.append($('<option></option>').attr('value', region.id).text(region.nombre));
              });

              userState = parseInt(userData.codigoDeptoResidencia);
              userCity = parseInt(userData.codigoCiudadResidencia);

              function loadCities(stateId, cityId) {
                var $cities = $('.cities');
                $cities.parent().addClass('loading');
                $cities.find('option:not(:first-of-type)').remove();
                _.httpRequest('https://proteccion.com.co/HubDigital/rest/departamentos/geografia/zonaGeografica/' + stateId, function(response) {
                  var cities = response.zonaGeograficas.map(function(ciudad) {return {id: ciudad.id, nombre: ciudad.nombre};});
                  cities.forEach(function(region) {
                    $cities.append($('<option></option>').attr('value', region.id).text(region.nombre));
                  });
                  $cities.parent().removeClass('loading');
                  if (cityId !== undefined) {
                    $cities.val(cityId);
                  } else {
                    $cities.val(-1);
                  }
                });
              }

              $('.states').on('change', function() {
                loadCities($(this).val());
              });

              $states.val(userState);
              loadCities(userState, userCity);
            }
          },
          {
            url: base + 'listaIngresos.json',
            afterResponse: function(response) {
              var $target = $('.incomes');
              response.forEach(function(income) {
                $target.append($('<option></option>').attr('value', income.codigo).text(income.descripcion));
              });
            }
          }, {
            url: base + 'listaEgresos.json',
            afterResponse: function(response) {
              var $target = $('.expenses');
              response.forEach(function(income) {
                $target.append($('<option></option>').attr('value', income.codigo).text(income.descripcion));
              });
            }
          }, {
            url: base + 'listaConceptoIngresos.json',
            afterResponse: function(response) {
              var $target = $('.income-concept');
              response.forEach(function(incomeConcept) {
                var incomeConceptElementMobile = '<div class="col-xs-6">' +
                '<input id="slider-checkbox-' + incomeConcept.descripcion.toLowerCase() + '" type="checkbox" class="input-slider input-slider-sm" value=' + incomeConcept.codigo + '>' +
                '<label for="slider-checkbox-' + incomeConcept.descripcion.toLowerCase() + '"><span></span>' + incomeConcept.descripcion + '</label>' +
                '</div>';
                var incomeConceptElementVideo = '<div class="col-sm-4">' +
                '<input id="video-checkbox-' + incomeConcept.descripcion.toLowerCase() + '" type="checkbox" class="input-video input-video-xs input-video-brown" value=' + incomeConcept.codigo + '>' +
                '<label for="video-checkbox-' + incomeConcept.descripcion.toLowerCase() + '"><span></span>' + incomeConcept.descripcion + '</label>' +
                '</div>';

                $target.eq(0).append(incomeConceptElementMobile);
                $target.eq(1).append(incomeConceptElementVideo);
              });
            }
          }, {
            url: base + 'listaOcupaciones.json',
            afterResponse: function(response) {
              var $target = $('.occupations');
              response.forEach(function(occupation) {
                $target.append($('<option></option>').attr('value', occupation.codigo).text(occupation.descripcion));
              });
            }
          }, {
            url: base + 'listaProfesiones.json',
            afterResponse: function(response) {
              var $target = $('.professions');
              response.forEach(function(profession) {
                $target.append($('<option></option>').attr('value', profession.codigo).text(profession.descripcion));
              });
            }
          }],
        url: 'php/receive_data.php',
        validations: [{
            name: 'states',
            trigger: {
              mobile: 10,
              desktop: 10
            },
            validation: function($target, setData) {
              var state = $target.val();
              if (state === '-1' || state === null) {
                return 'Por favor selecciona un departamento.';
              }
              setData({residencia: {departamento: state}});
              return false;
            }
          }, {
            name: 'cities',
            trigger: {
              mobile: 10,
              desktop: 10
            },
            validation: function($target, setData) {
              var city = $target.val();
              if (city === '-1' || city === null) {
                return 'Por favor selecciona una ciudad.';
              }
              setData({residencia: {ciudad: city}});
              return false;
            }
          }, {
            name: 'address',
            trigger: {
              mobile: 1,
              desktop: 1
            },
            validation: [
            function($target) {
              if ($target.val().length === 0) {
                return 'Por favor ingresar una dirección.';
              }
              return false;
            },
            function($target, setData) {
              var address = $target.val();
              if (address.length > 50) {
                return 'Recuerda que la direccion no debe exceder de 50 caracteres.';
              }
              setData({residencia: {direccion: address.toUpperCase()}});
              return false;
            }
            ],
            defaultValue: userData.direccionResidencia.toUpperCase()
          }, {
            name: 'email',
            trigger: {
              mobile: 1,
              desktop: 2
            },
            checkbox: '.email-checkbox',
            validation: function($target, setData) {
              var email = $target.val();
              if (!$target.parent('.validation-field').find('input:checkbox').is(':checked')) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (email.length === 0 || email === null) {
                  return 'Por favor ingresa un correo electrónico.';
                } else if (!re.test(email)) {
                  return 'Por favor ingresa un correo electrónico válido.';
                } else if (email.length > 50) {
                  return 'Por favor ingresa un email con menos de 50 caracteres.';
                }
              } else {
                email = false;
              }
              setData({correoElectronico: (email) ? email.toLowerCase() : email});
              return false;
            },
            defaultValue: userData.email
          }, {
            name: 'phone',
            trigger: {
              mobile: 1,
              desktop: 3
            },
            validation: function($target, setData) {
              var phoneNumber = $target.val();
              if (phoneNumber.length === 0) {
                return 'Por favor ingresa un número de celular.';
              } else if (phoneNumber.length < 7) {
                return 'Por favor ingresa un número con más de 6 caracteres.';
              } else if (phoneNumber.length > 12) {
                return 'Por favor ingresa un número con menos de 12 caracteres.';
              }
              setData({telefono: (phoneNumber !== null) ? parseInt(phoneNumber) : false});
              return false;
            },
            defaultValue: parseInt(userData.telefonoResidencia)
          }, {
            name: 'mobile',
            trigger: {
              mobile: 1,
              desktop: 3
            },
            validation: function($target, setData) {
              var mobilePhone = $target.val();
              if (!$target.parent('.validation-field').find('input:checkbox').is(':checked')) {
                if (mobilePhone.length === 0) {
                  return 'Por favor ingresa un número de celular.';
                } else if (mobilePhone.length < 10) {
                  return 'Por favor ingresa un número de celular con más de 9 caracteres.';
                } else if (mobilePhone.length > 12) {
                  return 'Por favor ingresa un número de celular con menos de 12 caracteres.';
                }
              } else {
                mobilePhone = false;
              }
              setData({celular: (mobilePhone) ? parseInt(mobilePhone) : mobilePhone});
              return false;
            },
            defaultValue: parseInt(userData.celular)
          }, {
            name: 'income-concept',
            trigger: {
              mobile: 2,
              desktop: 5
            },
            validation: function($target, setData) {
              var conceptIncomes = $target.find('input[type="checkbox"]:checked').map(function(i, element) {
                return element.value;
              });
              setData({financiero: {conceptoIngresos: conceptIncomes}});
              return false;
            }
          }, {
            name: 'professions',
            trigger: {
              mobile: 2,
              desktop: 5
            },
            validation: function($target, setData) {
              var profession = $target.val();
              if (profession === '-1' || profession === null) {
                return 'Por favor seleccionar una profesión.';
              }
              setData({financiero: {profesion: profession}});
              return false;
            },
            defaultValue: parseInt(userData.codigoProfesion)
          }, {
            name: 'occupations',
            trigger: {
              mobile: 2,
              desktop: 5
            },
            validation: function($target, setData) {
              var occupation = $target.val();
              if (occupation === '-1' || occupation === null) {
                return 'Por favor seleccionar una ocupación.';
              }
              setData({financiero: {ocupacion: occupation}});
              return false;
            },
            defaultValue: parseInt(userData.codigoOcupacion)
          }, {
            name: 'incomes',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var incomes = $target.val();
              if (incomes === '-1' || incomes === null) {
                return 'Por favor seleccionar ingresos.';
              }
              setData({financiero: {ingresos: incomes}});
              return false;
            },
            defaultValue: parseInt(userData.codigoIngresos)
          }, {
            name: 'expenses',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var expenses = $target.val();
              if (expenses === '-1' || expenses === null) {
                return 'Por favor selecciona egresos.';
              }
              setData({financiero: {egresos: expenses}});
              return false;
            },
            defaultValue: parseInt(userData.codigoEgresos)
          }, {
            name: 'totalAssets',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var totalAssets = $target.val();
              if (totalAssets.length === 0) {
                return 'Por favor ingresa activos.';
              } else if (totalAssets.length > 17) {
                return 'Por favor ingresas menos de 17 caracteres';
              }
              setData({financiero: {totalActivos: totalAssets}});
              return false;
            },
            defaultValue: parseInt(userData.totalActivos)
          }, {
            name: 'totalLiabilities',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var totalLiabilities = $target.val();
              if (totalLiabilities.length === 0) {
                return 'Por favor ingresa pasivos.';
              } else if (totalLiabilities.length > 17) {
                return 'Por favor ingresas menos de 17 caracteres';
              }
              setData({financiero: {totalPasivos: totalLiabilities}});
              return false;
            },
            defaultValue: parseInt(userData.totalPasivos)
          }, {
            name: 'authorization-mail',
            trigger: {
              mobile: 4,
              desktop: 4
            },
            validation: function($target, setData) {
              setData({autorizacion: {mail: ($target.val() === 'yes') ? true : false}});
              return false;
            }
          }, {
            name: 'authorization-sms',
            trigger: {
              mobile: 4,
              desktop: 4
            },
            validation: function($target, setData) {
              setData({autorizacion: {sms: ($target.val() === 'yes') ? true : false}});
              return false;
            }
          }],
      },
      targets: [{
          target: '#sound-advice',
          transition: [{
            time: 0,
            transition: 'show',
            afterEvent: function() {
              $('#sound-advice').addClass('bottom');
              this.audioFadeOut(0.18, 100);
            }
          }, {
            time: 184,
            transition: 'hide',
          }]
        }, {
          target: '#start-buttons',
          transition: [{
            time: 50.8,
            transition: 'show',
          }, {
            time: 51.5,
            transition: 'pause',
            beforeEvent: function(event) {
              var _ = this, $target = $(event.target);
              $target.find('.btn-start').one('click', function() {
                $target.removeClass('show');
                _.play();
              });
            }
          }]
        }, {
          target: '#name',
          transition: [{
            time: 52.0,
            transition: 'show',
            beforeEvent: function(event) {
              $(event.target).html(userData.nombreCompleto.split(' ')[0]);
            },
            afterEvent: function() {
              TweenLite.to('#name', 1.1, {opacity: 1});
            }
          }, {
            time: 55.2,
            afterEvent: function() {
              var tl = new TimelineMax();
              tl
                .to('#name', 0.6, {rotationY: '-=25', transformOrigin: '-10px center', ease: Power3.easeOut}, 'trigger')
                .to('#name', 0.9, {rotationY: '-=60', transformOrigin: '-10px center', opacity: 0, ease: Power2.easeOut})
                .to('#name', 0.5, {left: '+=8px', ease: Power2.easeOut}, 'trigger');
            }
          }]
        }, {
          target: '#kingdom',
          transition: [{
            step: 1,
            slider: 1,
            time: 101.2,
            transition: 'next'
          }, {
            time: 101.6,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#kingdom', 1, {opacity: 0, bottom: '3%', ease: Power3.easeOut});
              TweenLite.to('#kingdom', 1, {opacity: 1, bottom: '4%', ease: Power3.easeOut});
            }
          }, {
            time: 103.8,
            transition: 'pause-next',
          }],
        }, {
          target: '#email',
          transition: [{
            step: 2,
            slider: 1,
            time: 112.1,
            transition: 'next'
          }, {
            time: 112.4,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#email', 1, {opacity: 0, bottom: '28%'});
              TweenLite.to('#email', 1, {opacity: 1, bottom: '30%'});
            }
          }, {
            time: 114.5,
            transition: 'pause-next',
          }],
        }, {
          target: '#phones',
          transition: [{
            step: 3,
            slider: 1,
            time: 121.1,
            transition: 'next'
          }, {
            time: 121.5,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#phones', 1, {opacity: 0, bottom: '13%'});
              TweenLite.to('#phones', 1, {opacity: 1, bottom: '14%'});
            }
          }, {
            time: 126.1,
            transition: 'pause-next',
          }],
        }, {
          target: '#authorization',
          transition: [{
            step: 4,
            slider: 4,
            time: 137.7,
            transition: 'next'
          }, {
            time: 137.9,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#authorization', 0.8, {opacity: 0, bottom: '31%'});
              TweenLite.to('#authorization', 0.8, {opacity: 1, bottom: '32%'});
            }
          }, {
            time: 149.0,
            transition: 'pause-next'
          }]
        }, {
          target: '#financial',
          transition: [{
            step: 5,
            slider: 3,
            time: 167.3,
            delay: 1.2,
            transition: 'next',
          }, {
            time: 170.2,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#financial .paper', 0.8, {opacity: 0, top: '100%'});
              TweenLite.to('#financial .paper', 0.8, {opacity: 1, delay: 1, top: '0'});
            }
          }, {
            time: 174.7,
            transition: 'pause-next',
          }]
        }
        // , {afterEvent: function() {this.play(49);}, time: 0.5, transition: 'show', target: ''}
        ]
    };

    $('.udc-mask').updateDataCampaign(options);

  };

  loadCampaign = function(authenticationToken) {

    $.ajax({
      url: base + 'afiliado.json',
      type: 'GET',
      dataType: 'json',
      success: updateData,
      error: function() {console.error('Error de autenticación\nAuthentication token error');},
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-Auth-Token', authenticationToken);
      }
    });

  };

  loadCampaign('CC:123456789:0000');

}(jQuery.noConflict()));

/*
  @author: Jorge Alejandro Benjumea Giraldo
  @email: j.alejandro.benjumea@gmail.com
*/

// Grunt JSHINT and JSCS configuration
  /* globals define, module, require, TweenLite, Elastic */
  // jscs:disable maximumLineLength
  // jscs:disable jsDoc
  // jscs:disable disallowMultipleVarDecl

(function(factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery.noConflict());
  }

}(function($) {
  'use strict';

  var UpdateDataCampaign = window.UpdateDataCampaign || {};

  UpdateDataCampaign = (function() {

    function UpdateDataCampaign(element, settings) {

      var _ = this;

      _.defaults = {
        player: {
          audio: {
            file: ''
          },
          video: {
            file: '',
            poster: '',
            aspectRatio: '16:9'
          }
        }
      };

      _.initials = {
        isMobile: null,
        targets: [],
        events: [],
        eventQueue: [],
        currentEvent: {},
        currentStep: 0,
        lastEvent: {},
        $mask: $(element),
        $slider: null,
        media: 'slider',
        $player: null,
        player: null,
        form: null
      };

      $.extend(_, _.initials);

      _.options = $.extend(true, _.defaults, settings);

      _.loadEnd = $.proxy(_.loadEnd, _);

      _.init();

    }
    return UpdateDataCampaign;

  }());

  function Validation(settings) {
    var _ = this;

    _.defaults = {
      form: '',
      target: '',
      validation: []
    };

    _.initials = {
      isValid: null,
      isEnable: true,
      errorMessage: ''
    };

    $.extend(_, _.initials);

    _.options = $.extend({}, _.defaults, settings);

    _.renderError1 = $.proxy(_.renderError1, _);
    _.renderError2 = $.proxy(_.renderError2, _);

    _.init();
  }

  Validation.prototype.init = function() {
    var _ = this;

    if (_.options.hasOwnProperty('name')) {

      _.$target = $('[data-validation=' + _.options.name + ']').not('[type="checkbox"]');

      _.$target.on('change keypress keyup blur', function(event) {
        _.$target.not(this).val($(this).val());
        if (event.type === 'change') {
          _.validate().renderError();
        }
      });

      if (_.options.hasOwnProperty('defaultValue')) {
        _.$target.val(_.options.defaultValue);
      }
    }

    _.form = _.options.form;

    _.trigger = {
      mobile: _.options.trigger.mobile,
      desktop: _.options.trigger.desktop
    };

    if (_.options.validation.constructor === Array) {
      _.validations = _.options.validation.filter(function(element) {
        return element.constructor === Function;
      });
    } else if (_.options.validation.constructor === Function) {
      _.validations = [_.options.validation];
    } else {
      _.validations = [];
    }

    var $checkbox = $('[data-validation=' + this.options.name + ']').filter('[type="checkbox"]');

    if ($checkbox.length !== 0) {
      _.isEnable = !$checkbox.is(':checked');

      $checkbox.on('change', function() {
        var $this = $(this);
        _.isEnable = !$this.is(':checked');
        if (_.isEnable) {
          _.$target.prop('disabled', false);
          _.validate();
        } else {
          _.$target.val('').prop('disabled', true).parents('.validation-field').removeClass('error').find('.message').html('');
        }
        $checkbox.not(this).prop('checked', !_.isEnable);
      });

    }

    if (this.options.hasOwnProperty('afterInit')) {
      this.options.afterInit.call(this, _.$target);
    }
  };

  Validation.prototype.validate = function() {
    var _ = this;

    if (!_.isEnable) {
      _.isValid = true;
      return _;
    }

    _.isValid = !_.validations.some(function(validation) {
      var $target, setData;
      _.errorMessage = validation.apply(_, [$target = _.$target, setData = function(data) {_.form.setData(data);}]);
      return Boolean(_.errorMessage);
    });

    return _;
  };

  Validation.prototype.renderError = function() {
    var _ = this;

    if ($(window).width() <= 992) {
      _.renderError1.call();
    } else {
      _.renderError2.call();
    }

    return _;
  };

  Validation.prototype.renderError2 = function() {
    var _ = this;

    var $message = _.$target.parents('.validation-field').find('.message');

    function setMessage(message) {
      $message.html(message);
    }

    if (!_.isValid) {
      setMessage(_.errorMessage);

      _.$target.parents('.validation-field').addClass('error');

      TweenLite.to($message, 1, {opacity: 1, right: '104%', ease: Elastic. easeOut.config(1, 0.5)});

      setTimeout(function() {
        TweenLite.to($message, 1, {opacity: 0, right: '104%', ease: Elastic. easeOut.config(1, 1)});
      }, 3000);

    } else {
      _.$target.parents('.validation-field').removeClass('error');
      setMessage('');
      _.errorMessage = '';
    }

    return _;
  };

  Validation.prototype.renderError1 = function() {
    var _ = this;

    var $message = _.$target.parents('.validation-field').find('.message');

    function setMessage(message) {
      $message.html(message);
    }

    if (!_.isValid) {
      _.$target.parents('.validation-field').addClass('error');
    } else {
      _.$target.parents('.validation-field').removeClass('error');
      _.errorMessage = '';
    }
    setMessage(_.errorMessage);

    return _;
  };

  Validation.prototype.tip = function() {
  };

  Validation.prototype.validateAndRender = function() {
  };

  function Form(options) {
    this.options = $.extend(true, {
      data: {},
      httpRequest: [],
      url: '',
      validations: []
    }, options);
    this.data = {};
    this.url = '';
    this.validations = [];
    this.init();
  }

  Form.prototype.addValidation = function(validation) {
    this.validations.push(validation);
  };

  Form.prototype.httpRequest = function(url, callback) {
    var _ = this;

    $.getJSON(url, function(data) {
      callback.call(_, data);
    });

    return _;
  };

  Form.prototype.init = function() {
    var _ = this;
    this.data = this.options.data;
    this.url = this.options.url;

    this.options.httpRequest.forEach(function(httpRequest) {
      _.httpRequest(httpRequest.url, httpRequest.afterResponse);
    });

    this.options.validations.forEach(function(validationOptions) {
      _.addValidation(new Validation($.extend(true, validationOptions, {form: _})));
    });

    $('.numeric').on('keydown', function(event) {
      var $this = $(this);
      if ($.inArray(event.keyCode, [46,8,9,27,13,110]) !== -1 ||
        /65|67|86|88/.test(event.keyCode) && (!0 === event.ctrlKey || !0 === event.metaKey) ||
        35 <= event.keyCode && 40 >= event.keyCode) {
        return;
      }
      if ((event.shiftKey || 48 > event.keyCode || 57 < event.keyCode) && (96 > event.keyCode || 105 < event.keyCode)) {
        event.preventDefault();
      }
      $this.val($this.val().replace(/[^0-9]+/g, ''));
    });

    $('input.money').on('keyup', function() {
      var $this = $(this);
      $this.val($this.val().replace(/[^0-9]+/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','));
    });
  };

  Form.prototype.post = function(onSuccess) {
    $.ajax({
      data: JSON.stringify(this.data),
      url: this.url,
      method: 'POST',
      success: function() {
        if (onSuccess) {
          onSuccess.call();
        }
      },
      error: function() {
        console.error('Something happened sending the data');
      }
    });
  };

  Form.prototype.setData = function(data) {
    this.data = $.extend(true, this.data, data);
  };

  UpdateDataCampaign.prototype.init = function() {
    var _ = this;

    _.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

    if (_.isMobile) {
      _.loadStart().configSlider(_.loadEnd);
    } else {
      _.loadStart().configSlider().configPlayer(_.loadEnd);
    }

    _.form = new Form(_.options.form);
  };

  UpdateDataCampaign.prototype.loadStart = function() {
    var _ = this;

    $('html').addClass((_.isMobile ? 'isMobile' : 'isNotMobile') + ' udc udc-ON');

    setTimeout(function() {
      _.$mask.addClass('loading');
    }, 200);

    $(window).bind('beforeunload', function() {
      _.hide();
      return;
    });

    return _;
  };

  UpdateDataCampaign.prototype.loadEnd = function() {
    var _ = this;

    $('.player-poster').addClass('show');

    _.$mask.addClass('loaded').removeClass('loading');

    _.$audio[0].play();

    return _;
  };

  UpdateDataCampaign.prototype.show = function() {
    var _ = this;

    $('html').addClass('udc-ON');

    _.$mask
      .delay(500)
      .queue(function() {
        $(this).addClass('loading').dequeue();
      })
      .delay(800)
      .queue(function() {
        $(this).addClass('loaded').removeClass('loading').dequeue();
      });

    return _;
  };

  UpdateDataCampaign.prototype.configSlider = function(onComplete) {
    var _ = this, updateSize;

    updateSize = function() {
      if ($(window).width() <= 992) {
        _.$slider.css({
          'min-height': (_.$mask.height() - _.$mask.find('.udc-mobile-header').outerHeight(true) - _.$mask.find('.udc-mobile-footer').outerHeight(true)) + 'px'
        });
      }
    };

    _.$slider = $(_.options.slider.target)
      .slick($.extend(_.options.slider.options, {infinite: false, swipe: false}))
      .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.udc-mobile').removeClass('step-' + currentSlide);
        if (nextSlide >= 1 &&  nextSlide <= 4) {
          $('.dots li').removeClass('active');
          $('.dots li').eq(nextSlide - 1).addClass('active');
          $('.mobile').addClass('step-1-4');
        } else {
          $('.udc-mobile').removeClass('step-1-4');
        }
        if (nextSlide === 4) {
          $('.udc-mobile').find('.btn-next').html('Actualizar').attr('title', 'Actualizar');
        } else {
          $('.udc-mobile').find('.btn-next').html('Continuar').attr('title', 'Continuar');
        }
      })
      .on('afterChange', function(event, slick, currentSlide) {
        _.currentStep = currentSlide;
        $('.udc-mobile').addClass('step-' + currentSlide);
        if (currentSlide === 5) {
          _.post();
          setTimeout(function() {
            _.close();
          }, 6000);
        }
        updateSize();
      });

    $(_.options.mobileSection)
      .find('.btn-prev').on('click', function() {
        _.$slider.slick('slickPrev');
      }).end()
      .find('.btn-start').on('click', function() {
        _.$slider.slick('slickNext');
      }).end()
      .find('.btn-next').on('click', function() {
        _.goToNext();
      });

    $(window).on('resize', updateSize);

    _.$slider.slick('setPosition');

    updateSize();

    if (onComplete) {
      onComplete.call();
    }

    return _;
  };

  UpdateDataCampaign.prototype.configPlayer = function(onComplete) {
    var _ = this, lastWindowWidth, targets = _.options.targets || null, YTdeferred = $.Deferred(), timeupdater;

    if (_.isMobile) {
      return;
    }

    window.onYouTubeIframeAPIReady = function() {
      YTdeferred.resolve(window.YT);
    };

    function onProgress(currentTime) {
      var triggerTime = 0;

      if (_.nextEvent === undefined) {
        clearInterval(timeupdater);
        return;
      }

      if (_.nextEvent.hasOwnProperty('time')) {

        triggerTime = _.nextEvent.time;

        if (_.nextEvent.hasOwnProperty('delay')) {
          triggerTime += _.nextEvent.delay;
        }

        if (currentTime > triggerTime) {
          _.currentEvent = _.nextEvent;

          if (_.currentEvent.hasOwnProperty('step')) {
            _.currentStep = _.currentEvent.step;
          }

          if (_.currentEvent.hasOwnProperty('beforeEvent')) {
            _.currentEvent.beforeEvent.call(_, _.currentEvent);
          }

          if (_.currentEvent.hasOwnProperty('transition')) {

            switch (_.currentEvent.transition) {
              case 'show':
                $(_.currentEvent.target).addClass('show');
                break;

              case 'hide':
                $(_.currentEvent.target).removeClass('show');
                break;

              case 'pause':
                _.pause('waiting');
                break;

              case 'next':
                _.currentNext = _.currentEvent;
                $(_.currentEvent.target)
                  .find('.btn-prev').one('click', function() {
                    $(_.currentNext.target).removeClass('show');
                    _.goToPrev();
                  }).end()
                  .find('.btn-next').addClass('inactive');
                break;

              case 'pause-next':
                _.pause('waiting');
                _.player.status = 'waiting';
                $(_.currentNext.target)
                  .find('.btn-next')
                    .removeClass('inactive')
                    .off('click')
                    .on('click', function() {
                      if (_.goToNext()) {
                        $(_.currentNext.target).find('.btn');
                        $(this).parents('.show').addClass('hide').delay(500).removeClass('hide show');
                      }
                    }).end()
                  .find('.btn-update')
                    .on('click', function() {
                      if (_.goToNext()) {
                        _.post();
                        _.audioFadeOut(0, 100);
                      }
                    });
                break;

              case 'loop':
                _.player.currentTime(_.currentEvent.loopTime);
                break;

              default:
                console.error('Event not defined.');
            }
          }

          if (_.currentEvent.hasOwnProperty('afterEvent')) {
            _.currentEvent.afterEvent.call(_, _.currentEvent);
          }
          if (_.currentEvent.transition !== 'loop') {
            _.moveNextEvent();
          }
        }
      }
    }

    function resize() {
      var windowWidth = _.$mask.width();

      if (windowWidth <= 992 && lastWindowWidth > 992) {

        _.media = 'slider';

        if (_.currentNext !== undefined) {
          _.$slider.slick('slickGoTo', _.currentNext.slider, true);
        } else {
          _.$slider.slick('slickGoTo', 0, true);
        }

        if (_.player.status ===  'playing') {
          _.pause();
        }

        _.$audio[0].pause();

      } else if (windowWidth > 992) {

        _.setAspectRatio(_.options.desktopSection, _.options.player.video.aspectRatio);

        if (lastWindowWidth <= 992) {
          _.media = 'video';

          var videoStep, triggerTime;

          if (_.player.status ==  undefined) {
            _.$audio[0].play();
          }

          if (_.player.status ===  'waiting' && false) {

            _.form.validations
            .filter(function(validation) {
              return validation.trigger.mobile === _.$slider.slick('slickCurrentSlide');
            })
            .sort(function(a, b) {
              return (a.trigger.desktop > b.trigger.desktop) ? 1 : -1;
            })
            .some(function(validation) {
              if (!validation.validate().isValid) {
                videoStep = validation.trigger.desktop;
                return true;
              }
            });

            if (videoStep !== undefined) {

              triggerTime = _.events.filter(function(event) {
                if (event.step === videoStep) {
                  return true;
                }
              })
              .sort(function(a, b) {
                return (a.time > b.time) ? 1 : -1;
              })[0].time;

              _.eventQueue = _.createQueue().filter(function(event) {
                return _.findEvent(event).time >= triggerTime;
              });

              _.nextEvent = _.findEvent(_.eventQueue.shift());

              _.player.currentTime(triggerTime);

              _.$audio[0].play();

              _.play();
            }

          }
        }

      }

      lastWindowWidth = windowWidth;
    }

    function onPlayerReady(event) {
      var oldTime, videoTime;
      function updateTime() {
        oldTime = videoTime;
        if (_.player && _.player.getCurrentTime) {
          videoTime = _.player.getCurrentTime();
        }
        if (videoTime !== oldTime) {
          onProgress(videoTime);
        }
      }
      if (onComplete) {
        onComplete.call();
      }
      $('.ytp-hide-controls .ytp-watermark').css('display', 'none');
      timeupdater = setInterval(updateTime, 100);
    }

    function onPlayerStateChange(event) {
      if (event.data === 0) {
        _.player.stopVideo();
        _.close();
      }
    }

    YTdeferred.done(function(YT) {
      _.player = new YT.Player('video_udc', {
        height: '390',
        width: '640',
        videoId: _.options.player.videoId,
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        },
        playerVars: {
          color: 'white',
          controls: 0,
          fs: 0,
          hl: 'es',
          modestbranding: 1,
          rel: 0,
          showinfo: 0,
          start: 0
        }
      });
    });

    _.$audio = $(_.$mask.find('#audio_udc')[0]);
    _.$audio.attr('src', _.options.player.audioSrc).prop('volume', 0.8);

    _.media = ($(window).width() <= 992) ? 'slider' : 'video';

    $(_.options.desktopSection)
      .find('.btn-volume').data('volume', 100).on('click', function() {
        var $this, volume;
        $this = $(this);
        volume = $this.data('volume');
        switch (volume) {
          case 100:
            $this
              .attr('title', '80%')
              .data('volume', 80);
            _.$audio.prop('volume', 0.8);
            break;
          case 80:
            _.$audio.prop('volume', 0.6);
            $this
              .attr('title', '60%')
              .data('volume', 60);
            break;
          case 60:
            _.$audio.prop('volume', 0.4);
            $this
              .attr('title', '40%')
              .addClass('fa-volume-down')
              .removeClass('fa-volume-up')
              .data('volume', 40);
            break;
          case 40:
            _.$audio.prop('volume', 0.2);
            $this
              .attr('title', '20%')
              .data('volume', 20);
            break;
          case 20:
            _.$audio.prop('volume', 0.80);
            $this
              .attr('title', 'off')
              .addClass('fa-volume-off')
              .removeClass('fa-volume-down')
              .data('volume', 0);
            _.$audio[0].pause();
            break;
          case 0:
            $this
              .attr('title', '100%')
              .addClass('fa-volume-up')
              .removeClass('fa-volume-off')
              .data('volume', 100);
            _.$audio[0].play();
        }
      }).end()
      .find('.btn-close').on('click', function() {
        _.pause().hide();
        $('html').removeClass('player-ON');
      }).end()
      .find('.btn-play').on('click', function() {
        _.play();
      }).end()
      .find('.btn-start').one('click', function() {
        $(this).parents('.player-poster').removeClass('show');
        _.play();
      }).end()
      .find('.btn-dont-show-again').one('click', function() {
        _.close();
        if (_.options.dontShowAgain) {
          _.options.dontShowAgain.call(_);
        }
      }).end()
      .find('.btn-see-later').on('click', function() {
        _.hide();
        if (_.options.seeLater) {
          _.options.seeLater.call(_);
        }
      }).end();

    if ($.type(targets) === 'array' && targets.length) {
      targets.forEach(function(target) {
        _.addTarget(target);
      });
    }

    _.setAspectRatio(_.options.desktopSection, _.options.player.video.aspectRatio);

    _.nextEvent = _.findEvent(_.createQueue().shift());

    lastWindowWidth = $(window).width();
    $(window).on('resize', resize);

    return _;
  };

  UpdateDataCampaign.prototype.audioFadeIn = function(volumeLimit, interval) {
    var _ = this, volume, fadeIn;

    volume = _.$audio.prop('volume');

    fadeIn = setInterval(
      function() {
        if (volume <= volumeLimit) {
          volume += 0.01;
          _.$audio.prop('volume', volume);
        } else {
          clearInterval(fadeIn);
        }
      }, interval);

    return _;
  };

  UpdateDataCampaign.prototype.audioFadeOut = function(volumeLimit, interval) {
    var _ = this, volume, fadeout;

    volume = _.$audio.prop('volume');

    fadeout = setInterval(
      function() {
        if (volume - 0.01 >= volumeLimit) {
          volume -= 0.01;
          _.$audio.prop('volume', volume);
        } else {
          clearInterval(fadeout);
        }
      }, interval);

    return _;
  };

  UpdateDataCampaign.prototype.moveNextEvent = function() {
    var _ = this;

    _.nextEvent = _.findEvent(_.eventQueue.shift());
  };

  UpdateDataCampaign.prototype.addEvent = function(event) {
    var _ = this;

    _.events.push($.extend(event, {id: _.events.length}));
    _.events.sort(function(a, b) {return (a.time > b.time) ? 1 : -1;});

    return _;
  };

  UpdateDataCampaign.prototype.findEvent = function(eventId) {
    var _ = this;

    return _.events.filter(function(event) {
      return event.id === eventId;
    })[0];
  };

  UpdateDataCampaign.prototype.createQueue = function() {
    var _ = this;

    _.eventQueue.length = 0;

    _.events.forEach(function(event) {
      _.eventQueue.push(event.id);
    });

    return _.eventQueue;
  };

  UpdateDataCampaign.prototype.addTarget = function(target) {
    var _ = this;

    _.targets.push(target);

    if (target.transition.constructor === String) {
      _.addEvent(target);
    } else if (target.transition.constructor === Array) {
      target.transition.forEach(function(event) {
        _.addEvent($.extend(true, event, {target: target.target}));
      });
    }

    return _;
  };

  UpdateDataCampaign.prototype.addNextTargets = function(targets) {
    var _ = this;

    for (var key in targets) {
      _.addTarget($.extend(targets[key], {target: '#' + key, transition: 'next'}));
    }

    return _;
  };

  UpdateDataCampaign.prototype.pause = function(className) {
    var _ = this;

    _.player.pauseVideo();
    _.player.status = 'paused';
    $(_.options.desktopSection).removeClass('playing').addClass('paused ' + ((className === undefined) ? '' : className));

    return _;
  };

  UpdateDataCampaign.prototype.play = function(playerTime) {
    var _ = this;

    if ($(window).width() > 992) {
      if (playerTime !== undefined) {
        _.player.seekTo(playerTime, true);
      }
      _.$audio[0].play();
      _.player.playVideo();
      _.player.status = 'playing';
      $(_.options.desktopSection).removeClass('paused waiting').addClass('playing');
    }
  };

  UpdateDataCampaign.prototype.setAspectRatio = function(targetSelector, aspectRatio) {
    var $videoWrapper, videoWidth = [], videoHeight = [], width, height;

    aspectRatio = aspectRatio.split(':').map(function(num) {
      return parseInt(num, 10);
    });
    aspectRatio = aspectRatio[1] / aspectRatio[0];

    $videoWrapper = $(targetSelector).removeAttr('style');

    videoWidth[0] = Math.floor($videoWrapper.width());

    videoWidth[0] =  (videoWidth[0] > 940) ? 940 : videoWidth[0];

    while (((videoWidth[0] * aspectRatio) % 1 !== 0)) {
      videoWidth[0] --;
    }
    videoHeight[0] = videoWidth[0] * aspectRatio;

    // if (videoHeight[0] < $videoWrapper.height()) {
      width = videoWidth[0];
      height = videoHeight[0];
    // } else {
    //   videoHeight[1] = Math.floor($videoWrapper.height());
    //   while (((videoHeight[1] * (1 / aspectRatio)) % 1 !== 0)) {
    //     videoHeight[1] --;
    //   }
    //   videoWidth[1] = videoHeight[1] * (1 / aspectRatio);
    //   if (videoHeight[1] > 740) {
    //     width = videoWidth[1];
    //     height = videoHeight[1];
    //   } else {
    //     width = videoWidth[0];
    //     height = videoHeight[0];
    //   }
    // }

    $videoWrapper.width(width).height(height);
  };

  UpdateDataCampaign.prototype.destroy = function() {
    $('.udc').remove();
  };

  UpdateDataCampaign.prototype.close = function() {
    var _ = this;

    _.player.stopVideo();
    _.$audio[0].pause();

    _.$mask.removeClass('loading loaded');

    $('html').removeClass((_.isMobile ? 'isMobile' : 'isNotMobile') + ' udc udc-ON');

    _.destroy();

    return _;
  };

  UpdateDataCampaign.prototype.hide = function() {
    var _ = this;

    $('html').removeClass('udc-ON');

    _.$mask.removeClass('loading loaded');

    if (_.player.status ===  'playing' || _.player.status ===  'waiting') {
      _.pause();
    }
    _.$audio[0].pause();

    return _;
  };

  UpdateDataCampaign.prototype.post = function() {
    var _ = this;

    _.form.post(function() {
      console.info('Jorge Alejandro Benjumea Giraldo\nj.alejandro.benjumea@gmail.com\nFront-end developer\n');
    });

    return _;
  };

  UpdateDataCampaign.prototype.validate = function() {
    var _ = this, stepValidations, errors = [];

    stepValidations = _.form.validations.filter(function(validation) {
      if (_.media === 'slider') {
        if (validation.trigger.mobile === _.currentStep) {
          return true;
        }
      }
      if (_.media === 'video') {
        if (validation.trigger.desktop === _.currentStep) {
          return true;
        }
      }
    });

    stepValidations.forEach(function(validation, index) {
      errors.push(validation.validate().isValid);
      setTimeout(function() {
        validation.renderError();
      }, index * 130);
    });

    return (errors.indexOf(false) === -1) ? false : true;
  };

  UpdateDataCampaign.prototype.goToPrev = function() {
    var _ = this, indexEventId, nextEventIds;

    nextEventIds = _.createQueue().filter(function(eventId) {
      return _.findEvent(eventId).transition === 'next';
    });

    indexEventId = nextEventIds.indexOf(this.currentNext.id);

    if (indexEventId > 0) {
      _.eventQueue.splice(0, _.eventQueue.indexOf(_.findEvent(nextEventIds[indexEventId - 1]).id));
      _.nextEvent = _.findEvent(_.eventQueue.shift());
    } else {
      _.nextEvent = _.findEvent(_.eventQueue.shift());
    }

    _.player.seekTo(_.nextEvent.time);

    _.play();
  };

  UpdateDataCampaign.prototype.goToNext = function() {
    var _ = this;

    if (_.validate()) {
      return false;
    }

    if (_.media === 'slider') {
      _.$slider.slick('slickNext');
    } else {
      _.play();
    }

    return true;
  };

  $.fn.updateDataCampaign = function() {
    var _ = this,
    opt = arguments[0],
    args = Array.prototype.slice.call(arguments, 1),
    l = _.length,
    i,
    ret;
    for (i = 0; i < l; i++) {
      if (typeof opt === 'object' || typeof opt === 'undefined') {
        _[i].updateDataCampaign = new UpdateDataCampaign(_[i], opt);
      } else {
        // ret = _[i].slick[opt].apply(_[i].slick, args);
      }
      // if (typeof ret != 'undefined') return ret;
    }
    return _;
  };

}));
