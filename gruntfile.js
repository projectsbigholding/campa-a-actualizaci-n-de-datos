/* jshint node: true */
// jscs: disable maximumLineLength, requireCamelCaseOrUpperCaseIdentifiers

Number.prototype.padLeft = function(base, chr) { // jshint ignore:line
  'use strict';
  var  len = (String(base || 10).length - String(this).length) + 1;
  return len > 0 ? new Array(len).join(chr || '0') + this : this;
};

function dateFormat() {
  'use strict';
  var date = new Date();
  var dateFormat = {
    yyyymmdd: [date.getFullYear(), (date.getMonth() + 1).padLeft(), date.getDate().padLeft()].join('/'),
    hhmmss: [date.getHours().padLeft(), date.getMinutes().padLeft(), date.getSeconds().padLeft()].join(':')
  };
  return dateFormat;
}

function banner() {
  'use strict';
  var date = dateFormat();
  return '\n  author: ' + pkg.author + '\n  email: ' + pkg.email + '\n  Last update: ' + date.yyyymmdd + ' ' + date.hhmmss  + '\n';
}

function renameFile(dest, src) {
  'use strict';
  return dest + src.substring(src.indexOf('_') + 1);
}

var pkg = require('./package.json');

// Project settings.
var config = {
  developer: {
    directory: 'src/',
    css: 'src/css/',
    data: 'src/data/',
    sass: 'src/css/',
    less: 'src/css/less/',
    fonts: 'src/fonts/',
    img: 'src/img/',
    jade: 'src/jade/',
    js: 'src/js/',
    vendor: 'src/vendor/',
    php: 'src/php/'
  },
  production: {
    directory: 'dist/',
    css: 'dist/wps/contenthandler/dav/fs-type1/themes/TemaProteccion/css/videoActualizacionDatos/',
    data: 'dist/wps/contenthandler/dav/fs-type1/themes/TemaProteccion/js/videoActualizacionDatos/data/',
    sass: 'dist/wps/contenthandler/dav/fs-type1/themes/TemaProteccion/css/videoActualizacionDatos/',
    less: 'dist/wps/contenthandler/dav/fs-type1/themes/TemaProteccion/css/videoActualizacionDatos/',
    fonts: 'dist/fonts/',
    img: 'dist/img/',
    jade: 'dist/',
    js: 'dist/wps/contenthandler/dav/fs-type1/themes/TemaProteccion/js/videoActualizacionDatos/',
    vendor: 'dist/vendor/',
    php: 'dist/php/'
  },
  server: {
    port: 9000
  },
  livereload: {
    files: [
    //production.directory + '**/*',
    // '!**/*.{ai,psd,fw.png}'
    ],
    host: 'localhost',
    port: 35729
  },
  bower: {
  },
  notify: {
    title: 'Pásala Ganando'
  }
};

module.exports = function(grunt) {
  'use strict';

  // Load grunt tasks automatically.
  // https://www.npmjs.com/package/jit-grunt
  require('jit-grunt')(grunt, {
    bower: 'node_modules/grunt-bower-task/tasks/bower_task.js'
  });

  // Display the elapsed execution time of grunt tasks.
  // https://www.npmjs.com/package/time-grunt
  require('time-grunt')(grunt);

  // Override default message when the watch has finished running tasks.
  function watchDateFormat(time) {
    var date = dateFormat();
    grunt.log.writeln();
    grunt.log.ok(String('[' + date.yyyymmdd).green + ' ' + date.hhmmss.green.bold + ']'.green + ' Completed in ' + String(time.toFixed(3) + 's').green + ' - Waiting ...');
    grunt.log.writeln();
  }

  // Define the configuration for all the tasks.
  grunt.initConfig({

    // Project settings.
    config: config,

    // Run predefined tasks whenever watched file patterns are added, changed or deleted.
    watch: { /* https://www.npmjs.com/package/grunt-contrib-watch */
      options: {
        debounceDelay: 100, /* How long to wait before emitting events in succession for the same filepath and status. */
        dateFormat: watchDateFormat, /* Override message displayed when the watch has finished running tasks. */
        spawn: false
      },
      configurationFiles: {
        options: {
          event: 'changed', /* Specify the type of watch events that triggers the specified task. */
          reload: true /* Trigger the watch task to restart */
        },
        files: ['gruntfile.js'],
        tasks: ['jshint:gruntFile', 'jscs:gruntFile', 'notify:configurationFiles']
      },
      bower: {
        options: {
          event: ['changed']
        },
        files: ['bower.json', '.bowerrc'],
        tasks: ['bower:install', 'notify:bower']
      },
      css: {
        files: ['<%= config.developer.css %>**/*.css'],
        tasks: ['newer:copy:cssFolder', 'notify:css']
      },
      less: {
        files: ['<%= config.developer.less %>**/*.less'],
        tasks: ['newer:less:all', 'notify:less']
      },
      sass: {
        files: ['<%= config.developer.sass %>**/*.{scss,sass}'],
        tasks: ['sass', 'postcss:all', 'notify:sass']
      },
      data: {
        files: ['<%= config.developer.data %>**/*'],
        tasks: ['sync:dataFolder', 'notify:data']
      },
      fonts: {
        files: ['<%= config.developer.fonts %>**/*'],
        tasks: ['sync:fontsFolder', 'notify:fonts']
      },
      img: {
        files: ['<%= config.developer.img %>**/*'],
        tasks: ['sync:imgFolder', 'notify:img']
      },
      jade: {
        files: ['<%= config.developer.jade %>*.jade'],
        tasks: ['newer:jade:all', 'notify:jade']
      },
      jadeAll: {
        files: ['<%= config.developer.jade %>partials/*.jade', '<%= config.developer.jade %>layouts/*.jade'],
        tasks: ['jade:all', 'notify:jade']
      },
      js: {
        files: ['<%= config.developer.js %>**/*.js'],
        // tasks: ['newer:jshint:all', 'newer:jscs:all', 'newer:uglify:all', 'sync:jsFolder', 'notify:js']
        tasks: ['newer:jshint:all', 'newer:jscs:all', 'newer:uglify:all', 'notify:js']
      },
      vendor: {
        files: ['<%= config.developer.vendor %>**/*'],
        tasks: ['sync:vendorFolder', 'notify:vendor']
      },
      php: {
        files: ['<%= config.developer.php %>**/*'],
        tasks: ['sync:phpFolder', 'notify:php']
      },
      livereload: {
        options: {
          livereload: {
            host: '<%= config.livereload.host %>',
            port: '<%= config.livereload.port %>'
          }
        },
        files: ['<%= config.production.directory %>**/*']
      }
    },

    // Automatic desktop notifications for Grunt.
    notify: { /* https://github.com/dylang/grunt-notify */
      options: {
        enabled: true,
        max_jshint_notifications: 5, /* Maximum number of notifications from jshint output. */
        title: '<%= config.notify.title %>', /* Defaults to the name in package.json, or will use project directory's name. */
        success: false, /* Whether successful grunt executions should be notified automatically. */
        duration: 3 /* The duration of notification in seconds, for `notify-send only. */
      },
      configurationFiles: {
        options: {
          message: 'Configuration files changed',
        }
      },
      bower: {
        options: {
          title: 'Bower',
          message: 'Bower task done',
        }
      },
      css: {
        options: {
          title: 'CSS:',
          message: 'CSS task done',
        }
      },
      less: {
        options: {
          title: 'LESS:',
          message: 'Compiled successfully',
        }
      },
      sass: {
        options: {
          title: 'SASS:',
          message: 'Compiled successfully',
        }
      },
      data: {
        options: {
          title: '<%= config.production.data %>',
          message: 'Sync completed',
        }
      },
      fonts: {
        options: {
          title: '<%= config.production.fonts %>',
          message: 'Sync completed',
        }
      },
      img: {
        options: {
          title: '<%= config.production.img %>',
          message: '',
        }
      },
      jade: {
        options: {
          title: 'Jade:',
          message: 'Compilation finished',
        }
      },
      js: {
        options: {
          title: 'JavaScript Tasks',
          message: '<%= watch.js.tasks %>',
        }
      },
      vendor: {
        options: {
          title: '<%= config.production.vendor %>',
          message: 'Sync completed',
        }
      },
      php: {
        options: {
          title: '<%= config.production.php %>',
          message: 'Sync completed',
        }
      },
      build: {
        options: {
          title: '<%= config.production.php %>',
          message: 'Build completed!',
        }
      }
    },

    // Copy files and folders.
    copy: { /* https://www.npmjs.com/package/grunt-contrib-copy */
      cssFolder: {
        files: [{
          expand: true,
          flatten: true,
          filter: 'isFile',
          cwd: '<%= config.developer.css %>',
          src: ['**/*.css', '!**/*.{scss,sass,less}'],
          dest: '<%= config.production.css %>'
        }]
      },
      jsFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.js %>',
          src: ['*.min.js'],
          dest: '<%= config.production.js %>'
        }]
      }
    },

    // Apply several post-processors to your CSS
    postcss: { /* https://www.npmjs.com/package/grunt-postcss */
      options: {
        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({browsers: ['last 4 versions', 'ie 8', 'ie 9']}), // adds vendor prefixes, using data from Can I Use.
          require('cssnano')(), // minify the result
          require('postcss-banner')({banner: banner()})
       ],
        map: true,
        failOnError: true
      },
      all: {
        src: '<%= config.production.css %>*.css'
      }
    },

    // Compile LESS files to CSS.
    less: { /* https://www.npmjs.com/package/grunt-contrib-less */
      options: {
        banner: '/*' + banner() + '*/',
      },
      all: {
        files: [{
          expand: true,
          cwd: '<%= config.developer.less %>',
          src: ['*.less'],
          dest: '<%= config.production.less %>',
          ext: '.css'
        }]
      }
    },

    sass: {
      options: {
        style: 'expanded', /* Output style. Can be nested, compact, compressed, expanded. */
        compass: true,
        noCache: true
      },
      dist: {
        files: [{
          expand: true, /* Enable dynamic expansion. */
          cwd: '<%= config.developer.sass %>', /* Source matches are relative to this path. */
          src: ['*.sass', '*.scss'], /* Actual pattern(s) to match. */
          dest: '<%= config.production.sass %>', /* Destination path prefix. */
          // rename: rename,
          ext: '.min.css', /* Destination file paths will have this extension. */
          // extDot: 'first' /* Extensions in filenames begin after the first dot */
        }]
      }
    },

    // Synchronize two directories.
    sync: { /* https://www.npmjs.com/package/grunt-sync */
      dataFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.data %>',
          src: ['**/*'],
          dest: '<%= config.production.data %>'
        }],
        verbose: true,
        failOnError: true,
        updateAndDelete: true
      },
      fontsFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.fonts %>',
          src: ['**/*'],
          dest: '<%= config.production.fonts %>'
        }],
        verbose: true,
        failOnError: true,
        updateAndDelete: true
      },
      vendorFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.vendor %>',
          src: ['**/*'],
          dest: '<%= config.production.vendor %>'
        }],
        verbose: true,
        failOnError: true,
        updateAndDelete: true
      },
      imgFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.img %>',
          src: ['**/*', '!**/*.{ai,psd,fw.png}'],
          dest: '<%= config.production.img %>'
        }],
        verbose: true,
        failOnError: true,
        updateAndDelete: true
      },
      jsFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.js %>',
          src: ['**/*'],
          dest: '<%= config.production.js %>'
        }],
        verbose: true,
        failOnError: true,
        updateAndDelete: true
      },
      phpFolder: {
        files: [{
          expand: true,
          filter: 'isFile',
          cwd: '<%= config.developer.php %>',
          src: ['**/*', '!**/*.{zip,rar}'],
          dest: '<%= config.production.php %>'
        }],
        verbose: true,
        failOnError: true,
        updateAndDelete: true
      }
    },

    // Compile Jade templates.
    jade: { /* https://www.npmjs.com/package/grunt-contrib-jade */
      options: {
        pretty: true, /* Output indented HTML. */
        compileDebug: false
      },
      all: {
        files: [{
          filter: 'isFile',
          //dot: true,
          expand: true,
          flatten: true,
          cwd: '<%= config.developer.jade %>',
          src: ['**/*.jade', '!layouts/**/*', '!partials/**/*'],
          dest: '<%= config.production.jade %>',
          rename: renameFile,
          extDot: 'last',
          ext: '.html'
        }]
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes.
    jshint: { /* https://www.npmjs.com/package/grunt-contrib-jshint */
      options: {
        jshintrc: '.jshintrc', /* If set to true, no config will be sent to JSHint and JSHint will search for .jshintrc files relative to the files being linted. */
        force: true /* Set force to true to report JSHint errors but not fail the task. */
      },
      all: {
        src: ['<%= config.developer.js %>*.js', '!<%= config.developer.js %>**/*.min.js']
      },
      gruntFile: {
        src: ['gruntfile.js']
      }
    },

    // Check JavaScript Code Style.
    jscs: { /* https://www.npmjs.com/package/grunt-jscs */
      options: {
        config: '.jscsrc', /* Defines how to externally load a JSCS configuration via the JSCS config loader. */
        force: true /* Set force to true to report JSCS errors but not fail the task. */
      },
      all: {
        src: ['<%= config.developer.js %>*.js', '!<%= config.developer.js %>**/*.min.js']
      },
      gruntFile: {
        src: ['gruntfile.js']
      }
    },

    // Minify javascript files.
    uglify: { /* https://www.npmjs.com/package/grunt-contrib-uglify */
      options: {
        sourceMap: true,
        preserveComments: false, /* Turn on preservation of comments. */
        banner: '/*' + banner() + '*/', /* This string will be prepended to the minified output. */
        mangle: {
          except: ['jQuery', 'Backbone']
        }
      },
      all: {
        files: [{
          expand: true, /* Enable dynamic expansion. */
          cwd: '<%= config.developer.js %>', /* Source matches are relative to this path. */
          src: ['*.js', '!*.min.js', '!_*.js'], /* Actual pattern(s) to match. */
          dest: '<%= config.production.js %>', /* Destination path prefix. */
          rename: renameFile,
          ext: '.min.js', /* Destination file paths will have this extension. */
          extDot: 'first' /* Extensions in filenames begin after the first dot. */
        }]
      }
    },

    // Concatenate files.
    concat: { /* https://www.npmjs.com/package/grunt-contrib-concat */
      options: {
        banner: '/*' + banner() + '*/\n', /* This string will be prepended to the beginning of the concatenated output. */
      },
      js: {
        src: ['<%= config.developer.js %>*.js', '!<%= config.developer.js %>*.min.js', '!<%= config.developer.js %>_*.js'],
        dest: '<%= config.production.js %>' + 'main.js'
      },
      js_min: {
        options: {
          separator: '; ', /* Concatenated files will be joined on this string. */
          stripBanners: true /* Strip JavaScript banner comments from source files. */
        },
        src: ['<%= config.developer.js %>*min.js' + ''],
        dest: '<%= config.production.js %>' + 'main.min.js'
      },
    },

    // Clean files and folders.
    clean: { /* https://www.npmjs.com/package/grunt-contrib-clean */
      all: {src: '<%= config.production.directory %>*'},
      cssFolder: {src: '<%= config.production.css %>'}
    },

    // Compress files and folders.
    compress: { /* https://www.npmjs.com/package/grunt-contrib-compress */
      all: {
        options: {
          archive: pkg.name + '_V' + pkg.version + '.zip', /* Define where to output the archive. Each target can only have one output file. */
          pretty: true /* This is used to define which mode to use, currently supports gzip, deflate, deflateRaw, tar, tgz (tar gzip) and zip. */
        },
        files: [{
          expand: true,
          cwd: '<%= config.production.directory %>',
          src: ['**/*'],
        }]
      }
    },

  });

  // Task registration.
  grunt.registerTask('init', ['bower:install']);
  grunt.registerTask('build', [
    'clean:all',
    'copy:cssFolder',
    'less:all',
    'sass',
    'postcss:all',
    'sync:dataFolder',
    'sync:fontsFolder',
    'sync:imgFolder',
    'jade:all',
    'jshint:all',
    'jscs:all',
    'uglify:all',
    'concat',
    'sync:vendorFolder',
    'sync:phpFolder',
    'compress:all']);
  grunt.registerTask('default', ['watch']);

  // Adds a listener to the end of the listeners array for the specified event.
  grunt.event.on('watch', function(action, filepath, target) {
    function deleteFile(filePath) {
      if (filePath !== null) {
        if (grunt.file.delete(filePath)) {
          grunt.log.ok('File "' + filePath + '" deleted.');
        }
        grunt.task.clearQueue();
        grunt.task.run('watch');
      }
    }
    if (action === 'deleted') {
      var path = require('path');
      var file = null;
      switch (target) {
        case 'css':
          file = config.production.css + path.basename(filepath);
          break;
        case 'sass':
          file = config.production.sass + path.basename(filepath).replace('.scss', '.css');
          break;
        case 'less':
          file = config.production.less + path.basename(filepath).replace('.less', '.css');
          break;
        case 'jade':
          file = renameFile(config.production.jade, path.basename(filepath).replace('.jade', '.html'));
          break;
        default:
      }
      deleteFile(file);
    }
  });
};
