/*
  @author: Jorge Alejandro Benjumea Giraldo
  @email: j.alejandro.benjumea@gmail.com
*/

// Grunt JSHINT and JSCS configuration
  /* globals TimelineMax, TweenLite, Power2, Power3 */
  // jscs:disable maximumLineLength
  // jscs:disable disallowMultipleVarDecl

(function($) {
  'use strict';

  var updateData, loadCampaign, base;

  // base = 'wps/contenthandler/dav/fs-type1/themes/TemaProteccion/js/videoActualizacionDatos/data/';
  base = 'data/';

  updateData = function(userData) {

    var options = {
      mobileSection: '.udc-mobile',
      desktopSection: '.udc-player',
      slider: {
        target: '.udc-mobile-carousel',
        options: {
          prevArrow: '.btns .btn-prev',
          nextArrow: '.btns .btn-continue'
        }
      },
      player: {
        videoId: 'IrrI37HPtn8',
        audioSrc: 'player/proteccion.mp3'
      },
      dontShowAgain: function() {
        // 'No volver a mostrar' Button behavior
      },
      seeLater: function() {
        var _ = this;
        // 'Ver más tarde' Button behavior
        $('<h2><a style="color: blue; cursor:pointer;">Volver a mostrar</a><h2>')
          .on('click', function() {
            $(this).remove();
            _.show();
          })
          .insertAfter('.developer h1');
      },
      form: {
        data: {
          residencia: {
            ciudad: null,
            departamento: null,
            direccion: null,
          },
          telefono: null,
          celular: null,
          correoElectronico: null,
          autorizacion: {
            mail: false,
            sms: false
          },
          financiero: {
            conceptoIngresos: [],
            ingresos: null,
            egresos: null,
            totalActivos: null,
            totalPasivos: null,
            ocupacion: null,
            profesion: null
          }
        },
        httpRequest: [
          {
            url: base + 'regions.json',
            afterResponse: function(response) {
              var _ = this, $states, regions, userState, userCity;

              $states = $('.states');

              regions = response.regiones.map(function(region) {return {id: region.id.value, nombre: region.nombre.value};});
              regions.forEach(function(region) {
                $states.append($('<option></option>').attr('value', region.id).text(region.nombre));
              });

              userState = parseInt(userData.codigoDeptoResidencia);
              userCity = parseInt(userData.codigoCiudadResidencia);

              function loadCities(stateId, cityId) {
                var $cities = $('.cities');
                $cities.parent().addClass('loading');
                $cities.find('option:not(:first-of-type)').remove();
                // _.httpRequest('https://proteccion.com.co/HubDigital/rest/departamentos/geografia/zonaGeografica/' + stateId, function(response) {
                //   var cities = response.zonaGeograficas.map(function(ciudad) {return {id: ciudad.id, nombre: ciudad.nombre};});
                //   cities.forEach(function(region) {
                //     $cities.append($('<option></option>').attr('value', region.id).text(region.nombre));
                //   });
                  $cities.parent().removeClass('loading');
                //   if (cityId !== undefined) {
                //     $cities.val(cityId);
                //   } else {
                //     $cities.val(-1);
                //   }
                // });
              }

              $('.states').on('change', function() {
                loadCities($(this).val());
              });

              $states.val(userState);
              loadCities(userState, userCity);
            }
          },
          {
            url: base + 'listaIngresos.json',
            afterResponse: function(response) {
              var $target = $('.incomes');
              response.forEach(function(income) {
                $target.append($('<option></option>').attr('value', income.codigo).text(income.descripcion));
              });
            }
          }, {
            url: base + 'listaEgresos.json',
            afterResponse: function(response) {
              var $target = $('.expenses');
              response.forEach(function(income) {
                $target.append($('<option></option>').attr('value', income.codigo).text(income.descripcion));
              });
            }
          }, {
            url: base + 'listaConceptoIngresos.json',
            afterResponse: function(response) {
              var $target = $('.income-concept');
              response.forEach(function(incomeConcept) {
                var incomeConceptElementMobile = '<div class="col-xs-6">' +
                '<input id="slider-checkbox-' + incomeConcept.descripcion.toLowerCase() + '" type="checkbox" class="input-slider input-slider-sm" value=' + incomeConcept.codigo + '>' +
                '<label for="slider-checkbox-' + incomeConcept.descripcion.toLowerCase() + '"><span></span>' + incomeConcept.descripcion + '</label>' +
                '</div>';
                var incomeConceptElementVideo = '<div class="col-sm-4">' +
                '<input id="video-checkbox-' + incomeConcept.descripcion.toLowerCase() + '" type="checkbox" class="input-video input-video-xs input-video-brown" value=' + incomeConcept.codigo + '>' +
                '<label for="video-checkbox-' + incomeConcept.descripcion.toLowerCase() + '"><span></span>' + incomeConcept.descripcion + '</label>' +
                '</div>';

                $target.eq(0).append(incomeConceptElementMobile);
                $target.eq(1).append(incomeConceptElementVideo);
              });
            }
          }, {
            url: base + 'listaOcupaciones.json',
            afterResponse: function(response) {
              var $target = $('.occupations');
              response.forEach(function(occupation) {
                $target.append($('<option></option>').attr('value', occupation.codigo).text(occupation.descripcion));
              });
            }
          }, {
            url: base + 'listaProfesiones.json',
            afterResponse: function(response) {
              var $target = $('.professions');
              response.forEach(function(profession) {
                $target.append($('<option></option>').attr('value', profession.codigo).text(profession.descripcion));
              });
            }
          }],
        url: 'php/receive_data.php',
        validations: [{
            name: 'states',
            trigger: {
              mobile: 1,
              desktop: 1
            },
            validation: function($target, setData) {
              var state = $target.val();
              if (state === '-1' || state === null) {
                return 'Por favor selecciona un departamento.';
              }
              setData({residencia: {departamento: state}});
              return false;
            }
          }, {
            name: 'cities',
            trigger: {
              mobile: 10,
              desktop: 10
            },
            validation: function($target, setData) {
              var city = $target.val();
              if (city === '-1' || city === null) {
                return 'Por favor selecciona una ciudad.';
              }
              setData({residencia: {ciudad: city}});
              return false;
            }
          }, {
            name: 'address',
            trigger: {
              mobile: 1,
              desktop: 1
            },
            validation: [
            function($target) {
              if ($target.val().length === 0) {
                return 'Por favor ingresar una dirección.';
              }
              return false;
            },
            function($target, setData) {
              var address = $target.val();
              if (address.length > 50) {
                return 'Recuerda que la direccion no debe exceder de 50 caracteres.';
              }
              setData({residencia: {direccion: address.toUpperCase()}});
              return false;
            }
            ],
            defaultValue: userData.direccionResidencia.toUpperCase()
          }, {
            name: 'email',
            trigger: {
              mobile: 1,
              desktop: 2
            },
            checkbox: '.email-checkbox',
            validation: function($target, setData) {
              var email = $target.val();
              if (!$target.parent('.validation-field').find('input:checkbox').is(':checked')) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (email.length === 0 || email === null) {
                  return 'Por favor ingresa un correo electrónico.';
                } else if (!re.test(email)) {
                  return 'Por favor ingresa un correo electrónico válido.';
                } else if (email.length > 50) {
                  return 'Por favor ingresa un email con menos de 50 caracteres.';
                }
              } else {
                email = false;
              }
              setData({correoElectronico: (email) ? email.toLowerCase() : email});
              return false;
            },
            defaultValue: userData.email
          }, {
            name: 'phone',
            trigger: {
              mobile: 1,
              desktop: 3
            },
            validation: function($target, setData) {
              var phoneNumber = $target.val();
              if (phoneNumber.length === 0) {
                return 'Por favor ingresa un número de celular.';
              } else if (phoneNumber.length < 7) {
                return 'Por favor ingresa un número con más de 6 caracteres.';
              } else if (phoneNumber.length > 12) {
                return 'Por favor ingresa un número con menos de 12 caracteres.';
              }
              setData({telefono: (phoneNumber !== null) ? parseInt(phoneNumber) : false});
              return false;
            },
            defaultValue: parseInt(userData.telefonoResidencia)
          }, {
            name: 'mobile',
            trigger: {
              mobile: 1,
              desktop: 3
            },
            validation: function($target, setData) {
              var mobilePhone = $target.val();
              if (!$target.parent('.validation-field').find('input:checkbox').is(':checked')) {
                if (mobilePhone.length === 0) {
                  return 'Por favor ingresa un número de celular.';
                } else if (mobilePhone.length < 10) {
                  return 'Por favor ingresa un número de celular con más de 9 caracteres.';
                } else if (mobilePhone.length > 12) {
                  return 'Por favor ingresa un número de celular con menos de 12 caracteres.';
                }
              } else {
                mobilePhone = false;
              }
              setData({celular: (mobilePhone) ? parseInt(mobilePhone) : mobilePhone});
              return false;
            },
            defaultValue: parseInt(userData.celular)
          }, {
            name: 'income-concept',
            trigger: {
              mobile: 2,
              desktop: 5
            },
            validation: function($target, setData) {
              var conceptIncomes = $target.find('input[type="checkbox"]:checked').map(function(i, element) {
                return element.value;
              });
              setData({financiero: {conceptoIngresos: conceptIncomes}});
              return false;
            }
          }, {
            name: 'professions',
            trigger: {
              mobile: 2,
              desktop: 5
            },
            validation: function($target, setData) {
              var profession = $target.val();
              if (profession === '-1' || profession === null) {
                return 'Por favor seleccionar una profesión.';
              }
              setData({financiero: {profesion: profession}});
              return false;
            },
            defaultValue: parseInt(userData.codigoProfesion)
          }, {
            name: 'occupations',
            trigger: {
              mobile: 2,
              desktop: 5
            },
            validation: function($target, setData) {
              var occupation = $target.val();
              if (occupation === '-1' || occupation === null) {
                return 'Por favor seleccionar una ocupación.';
              }
              setData({financiero: {ocupacion: occupation}});
              return false;
            },
            defaultValue: parseInt(userData.codigoOcupacion)
          }, {
            name: 'incomes',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var incomes = $target.val();
              if (incomes === '-1' || incomes === null) {
                return 'Por favor seleccionar ingresos.';
              }
              setData({financiero: {ingresos: incomes}});
              return false;
            },
            defaultValue: parseInt(userData.codigoIngresos)
          }, {
            name: 'expenses',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var expenses = $target.val();
              if (expenses === '-1' || expenses === null) {
                return 'Por favor selecciona egresos.';
              }
              setData({financiero: {egresos: expenses}});
              return false;
            },
            defaultValue: parseInt(userData.codigoEgresos)
          }, {
            name: 'totalAssets',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var totalAssets = $target.val();
              if (totalAssets.length === 0) {
                return 'Por favor ingresa activos.';
              } else if (totalAssets.length > 17) {
                return 'Por favor ingresas menos de 17 caracteres';
              }
              setData({financiero: {totalActivos: totalAssets}});
              return false;
            },
            defaultValue: parseInt(userData.totalActivos)
          }, {
            name: 'totalLiabilities',
            trigger: {
              mobile: 3,
              desktop: 5
            },
            validation: function($target, setData) {
              var totalLiabilities = $target.val();
              if (totalLiabilities.length === 0) {
                return 'Por favor ingresa pasivos.';
              } else if (totalLiabilities.length > 17) {
                return 'Por favor ingresas menos de 17 caracteres';
              }
              setData({financiero: {totalPasivos: totalLiabilities}});
              return false;
            },
            defaultValue: parseInt(userData.totalPasivos)
          }, {
            name: 'authorization-mail',
            trigger: {
              mobile: 4,
              desktop: 4
            },
            validation: function($target, setData) {
              setData({autorizacion: {mail: ($target.val() === 'yes') ? true : false}});
              return false;
            }
          }, {
            name: 'authorization-sms',
            trigger: {
              mobile: 4,
              desktop: 4
            },
            validation: function($target, setData) {
              setData({autorizacion: {sms: ($target.val() === 'yes') ? true : false}});
              return false;
            }
          }],
      },
      targets: [{
          target: '#sound-advice',
          transition: [{
            time: 0,
            transition: 'show',
            afterEvent: function() {
              $('#sound-advice').addClass('bottom');
              this.audioFadeOut(0.18, 100);
            }
          }, {
            time: 184,
            transition: 'hide',
          }]
        }, {
          target: '#start-buttons',
          transition: [{
            time: 50.8,
            transition: 'show',
          }, {
            time: 51.5,
            transition: 'pause',
            beforeEvent: function(event) {
              var _ = this, $target = $(event.target);
              $target.find('.btn-start').one('click', function() {
                $target.removeClass('show');
                _.play();
              });
            }
          }]
        }, {
          target: '#name',
          transition: [{
            time: 52.0,
            transition: 'show',
            beforeEvent: function(event) {
              $(event.target).html(userData.nombreCompleto.split(' ')[0]);
            },
            afterEvent: function() {
              TweenLite.to('#name', 1.1, {opacity: 1});
            }
          }, {
            time: 55.2,
            afterEvent: function() {
              var tl = new TimelineMax();
              tl
                .to('#name', 0.6, {rotationY: '-=25', transformOrigin: '-10px center', ease: Power3.easeOut}, 'trigger')
                .to('#name', 0.9, {rotationY: '-=60', transformOrigin: '-10px center', opacity: 0, ease: Power2.easeOut})
                .to('#name', 0.5, {left: '+=8px', ease: Power2.easeOut}, 'trigger');
            }
          }]
        }, {
          target: '#kingdom',
          transition: [{
            step: 1,
            slider: 1,
            time: 101.2,
            transition: 'next'
          }, {
            time: 101.6,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#kingdom', 1, {opacity: 0, bottom: '3%', ease: Power3.easeOut});
              TweenLite.to('#kingdom', 1, {opacity: 1, bottom: '4%', ease: Power3.easeOut});
            }
          }, {
            time: 103.8,
            transition: 'pause-next',
          }],
        }, {
          target: '#email',
          transition: [{
            step: 2,
            slider: 1,
            time: 112.1,
            transition: 'next'
          }, {
            time: 112.4,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#email', 1, {opacity: 0, bottom: '28%'});
              TweenLite.to('#email', 1, {opacity: 1, bottom: '30%'});
            }
          }, {
            time: 114.5,
            transition: 'pause-next',
          }],
        }, {
          target: '#phones',
          transition: [{
            step: 3,
            slider: 1,
            time: 121.1,
            transition: 'next'
          }, {
            time: 121.5,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#phones', 1, {opacity: 0, bottom: '13%'});
              TweenLite.to('#phones', 1, {opacity: 1, bottom: '14%'});
            }
          }, {
            time: 126.1,
            transition: 'pause-next',
          }],
        }, {
          target: '#authorization',
          transition: [{
            step: 4,
            slider: 4,
            time: 137.7,
            transition: 'next'
          }, {
            time: 137.9,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#authorization', 0.8, {opacity: 0, bottom: '31%'});
              TweenLite.to('#authorization', 0.8, {opacity: 1, bottom: '32%'});
            }
          }, {
            time: 149.0,
            transition: 'pause-next'
          }]
        }, {
          target: '#financial',
          transition: [{
            step: 5,
            slider: 3,
            time: 167.3,
            delay: 1.2,
            transition: 'next',
          }, {
            time: 170.2,
            transition: 'show',
            afterEvent: function() {
              TweenLite.from('#financial .paper', 0.8, {opacity: 0, top: '100%'});
              TweenLite.to('#financial .paper', 0.8, {opacity: 1, delay: 1, top: '0'});
            }
          }, {
            time: 174.7,
            transition: 'pause-next',
          }]
        }
        // , {afterEvent: function() {this.play(49);}, time: 0.5, transition: 'show', target: ''}
        ]
    };

    $('.udc-mask').updateDataCampaign(options);

  };

  loadCampaign = function(authenticationToken) {

    $.ajax({
      url: base + 'afiliado.json',
      type: 'GET',
      dataType: 'json',
      success: updateData,
      error: function() {console.error('Error de autenticación\nAuthentication token error');},
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-Auth-Token', authenticationToken);
      }
    });

  };

  loadCampaign('CC:123456789:0000');

}(jQuery.noConflict()));
