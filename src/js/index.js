/*
  @author: Jorge Alejandro Benjumea Giraldo
  @email: j.alejandro.benjumea@gmail.com
*/

$(function() {

  'use strict';

  $.ajax({
    url: 'udc.html',
    type: 'GET',
    dataType: 'html',
    success: function(data) {
      $(data).insertBefore($('body > script').eq(0));
    },
    error: function() {
      console.error('Update Data Campaign template not found');
    }
  });

});
